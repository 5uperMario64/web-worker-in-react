import React, { Component } from 'react';

export class Countdown extends Component {

    worker = undefined;

    startWorker = () => {
        console.log("startWorker() started");
        if (typeof (Worker) !== "undefined") {
            if (typeof (this.worker) == "undefined") {
                console.log("this.worker is undefined");
                this.worker = new Worker(__dirname + '/CountdownWorker.js');    // TODO: Why is this not working
                console.log(this.worker);
            }
            console.log("this.worker is defined");
            this.worker.onmessage = function(event) {
                document.getElementById("result").innerHTML = event.data;
            };
        } else {
            document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Workers...";
        }
    }

    render() {
        return (
            <div>
                <h2>Count down: <output id="result"></output></h2>
                <button onClick={this.startWorker}>Start</button>
                <button>Stop</button>
            </div>
        );
    }
}