var i = 10;

function timedCountdown() {
  console.log("timedCountdown() started");
  if(i >= 0) {
    i--;
    postMessage(i);
    setTimeout(timedCountdown(),500); 
  }
  return;
}

timedCountdown(); 