import React from 'react';
import './App.css';
import { Countdown } from './Countdown.js'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Web Worker in React</h1>
        <Countdown />
      </header>      
    </div>
  );
}

export default App;
